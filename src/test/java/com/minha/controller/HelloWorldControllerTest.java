package com.minha.controller;

import com.minha.dao.HelloWorld;
import com.minha.handler.HelloHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebFluxTest
@Import(value = HelloHandler.class)
public class HelloWorldControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void test_HelloWorldRouter() {


        HelloWorld responseBody = webTestClient.get().uri("/hello").exchange()
                //상태 200
                .expectStatus().isOk()
                //객체 확인
                .expectBody(HelloWorld.class)
                .returnResult().getResponseBody();

        assertEquals(responseBody.getId(), 1L);
        assertEquals(responseBody.getTitle(), "hello");
        assertEquals(responseBody.getMessage(), "hi");

    }

    @Test
    public void test_FailExcutePostMethod(){
        webTestClient.post().uri("/hello").exchange()
                .expectStatus().isEqualTo(HttpStatus.METHOD_NOT_ALLOWED);
    }




}
package com.minha.dao;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HelloWorld {
    private long id;
    private String title;
    private String message;
}

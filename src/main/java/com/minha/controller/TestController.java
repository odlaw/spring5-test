package com.minha.controller;

import com.minha.dao.HelloWorld;
import com.minha.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping("/hello1")
    public HelloWorld find(){
        return testService.getHelloWorld();
    }

    @GetMapping("/hello2")
    public Mono<HelloWorld> findReactive(){
        return Mono.just(testService.getHelloWorld());
    }

    @GetMapping("/hello3")
    public List<HelloWorld> findList(){

        List<HelloWorld> helloWorldList = new ArrayList<>();

        helloWorldList.add(new HelloWorld(0L, "hello", "hi"));
        helloWorldList.add(new HelloWorld(1L, "hello", "hi"));
        helloWorldList.add(new HelloWorld(2L, "hello", "hi"));

        return helloWorldList;
    }

    @GetMapping("webClient/mono")
    public Mono<HelloWorld> clientMonoTest(
            @RequestParam("number") String number
    ){

        WebClient webClient = WebClient.create("http://localhost:8080");

        Mono<HelloWorld> returnValue = webClient.get()
                .uri("hello" + number)
                .retrieve()
                .bodyToMono(HelloWorld.class);
        return returnValue;
    }

    @GetMapping("webClient/flux")
    public Flux<HelloWorld> clientFluxTest(
    ){

        WebClient webClient = WebClient.create("http://localhost:8080");

        Flux<HelloWorld> returnValue = webClient.get()
            .uri("hello3")
            .exchange()
            .flatMapMany(
                    clientResponse -> clientResponse.bodyToFlux(HelloWorld.class)
            );

        return returnValue;
    }

}

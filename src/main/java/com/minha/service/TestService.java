package com.minha.service;

import com.minha.dao.HelloWorld;
import org.springframework.stereotype.Service;

@Service
public class TestService {

    public HelloWorld getHelloWorld(){
        return new HelloWorld(0L, "hello", "hi");
    }


}

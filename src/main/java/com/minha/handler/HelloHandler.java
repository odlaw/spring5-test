package com.minha.handler;

import com.minha.dao.HelloWorld;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
public class HelloHandler {

    public Mono<ServerResponse> find(ServerRequest request){
        Mono<HelloWorld> helloWorldMono = Mono.just(new HelloWorld(1L, "hello", "hi"));
        return ServerResponse.ok().body(helloWorldMono, HelloWorld.class);
    }

}
